import static java.lang.Integer.max;

public class Letters {
    public static final String EMPTY_STRING = "";
    public static final String ONE_WHITESPACE = " ";
    public static final String TWO_WHITESPACES = "  ";
    public static final String NEWLINE = "\n";

    public String print(String letter) {
        String alphabet = "abcdefghij";
        String[] alphabetToArray = alphabet.split(EMPTY_STRING);
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < alphabet.indexOf(letter) + 1; i++) {
            String innerSpacer = ONE_WHITESPACE + TWO_WHITESPACES.repeat(max(0, i - 1));
            String outerSpacer = ONE_WHITESPACE.repeat(alphabet.indexOf(letter)).substring(i);
            if (isFirstLine(i)) {
                result.append(outerSpacer).append(alphabetToArray[i]).append(outerSpacer);
            } else if (isMiddleOfDiamond(letter, alphabetToArray[i])) {
                result.append(NEWLINE).append(alphabetToArray[i]).append(innerSpacer).append(alphabetToArray[i]);
            } else {
                result.append(NEWLINE).append(outerSpacer).append(alphabetToArray[i]).append(innerSpacer).append(alphabetToArray[i]).append(outerSpacer);
            }
        }

        String diamondTop = result.toString();
        String diamondBottom = result.reverse().substring(max(1, result.indexOf(NEWLINE)));

        return (diamondTop + diamondBottom).toUpperCase();
    }

    private boolean isMiddleOfDiamond(String letter, String s) {
        return s.equals(letter);
    }

    private Boolean isFirstLine(int index) {
        return index == 0;
    }
}