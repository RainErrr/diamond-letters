import org.junit.Assert;
import org.junit.Test;

public class LettersTest {
    Letters letters = new Letters();

    @Test
    public void print_A() {
        Assert.assertEquals("A", letters.print("a"));
    }

    @Test
    public void print_AB() {
        String expected =
                        " A \n" +
                        "B B\n" +
                        " A ";
        Assert.assertEquals(expected, letters.print("b"));
    }

    @Test
    public void print_ABC() {
        String expected =
                        "  A  \n" +
                        " B B \n" +
                        "C   C\n" +
                        " B B \n" +
                        "  A  ";
        Assert.assertEquals(expected, letters.print("c"));
    }

    @Test
    public void print_ABCD() {
        String expected =
                        "   A   \n" +
                        "  B B  \n" +
                        " C   C \n" +
                        "D     D\n" +
                        " C   C \n" +
                        "  B B  \n" +
                        "   A   ";
        Assert.assertEquals(expected, letters.print("d"));
    }

    @Test
    public void print_ABCDE() {
        String expected =
                        "    A    \n" +
                        "   B B   \n" +
                        "  C   C  \n" +
                        " D     D \n" +
                        "E       E\n" +
                        " D     D \n" +
                        "  C   C  \n" +
                        "   B B   \n" +
                        "    A    ";
        Assert.assertEquals(expected, letters.print("e"));
    }
}